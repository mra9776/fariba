
#include <SFML/Graphics.hpp>

#include "lib/World.h"
#include "lib/WorldMonitor.h"
#include "lib/Rock.h"

int main2(){
  sf::RenderWindow window(sf::VideoMode(200, 200), "SFML Works!");
  sf::CircleShape shape(100.f);
  shape.setFillColor(sf::Color::Green);

  while(window.isOpen()){
    sf::Event event;
    while(window.pollEvent(event)){
      if (event.type == sf::Event::Closed)
        window.close();

    }

    window.clear();
    window.draw(shape);
    window.display();

  }
  return 0;
}

int main(){
  sf::RenderWindow window(sf::VideoMode(400, 400), "SFML Works!");

  World world;
  WorldMonitor worldMonitor(&world, &window);

  Rock obstacle("ROCK"); 
  world.addItem(&obstacle, Point(1, 1));

  Rock obstacle2("ROCK"); 
  world.addItem(&obstacle2, Point(2, 1));

  while(window.isOpen()){
    sf::Event event;
    while(window.pollEvent(event)){
      if (event.type == sf::Event::Closed)
        window.close();

    }

    worldMonitor.render();
    world.update();

    

  }
}