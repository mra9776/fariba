#include "../lib/World.h"
#include "../lib/Obstacle.h"
#include "../lib/Point.h"

#define CATCH_CONFIG_MAIN // This tells Catch to provide a main() - only do this in one cpp file
#include "catch.hpp"

TEST_CASE("Init world", "[world]")
{
    World world;
    REQUIRE(world.get_number() == 42);
    
}

TEST_CASE("Add Object to world", "[world]"){
    World world;
    Rock obstacle("ROCK");
    world.addItem(&obstacle, Point(1, 2));
    REQUIRE(world.getItems().size() == 1);
    REQUIRE(world.getItems()[0].item->getName() == "ROCK");
}

TEST_CASE("Init an object", "[object]"){
    Rock obstacle("ROCK");
    REQUIRE(obstacle.getName() == "ROCK");
}