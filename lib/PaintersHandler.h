#include <SFML/Graphics.hpp>

#include <string>
#include <unordered_map>

#include "Painter.h"
#include "PainterRock.h"
using std::unordered_map;
using std::string;

class PaintersHandler{
    public:
    PaintersHandler(sf::RenderWindow* _window){
        window = _window;
        initial();
    };

    void initial(){
        painters[PainterRock::getSign()] = new PainterRock(window);
    }

    void paint(ObjectMetadata* objectMetadata){
        painters[objectMetadata->item->getName()]->paint(objectMetadata);
    }
private:
    unordered_map<string, Painter *> painters;
    sf::RenderWindow* window;
};