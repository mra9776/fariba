#include "Painter.h"

class PainterRock : public Painter
{
public:
    static string getSign() {
        return "ROCK";
    }

    PainterRock(sf::RenderWindow *_window) : Painter(_window) {}

    void paint(ObjectMetadata *object)
    {
        sf::CircleShape shape(50.f);
        shape.setFillColor(sf::Color(100, 250, 50));
        shape.move(
            object->position.getX() * Painter::WIDTH_MULTIPLIER,
            object->position.getY() * Painter::WIDTH_MULTIPLIER);
        Painter::window->draw(shape);
    }
};