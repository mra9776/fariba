#pragma once

#include <vector>
using std::vector;

#include "Object.h"
#include "ObjectMetadata.h"
#include "Point.h"

class World
{
public:
    int get_number();
    void addItem(Object* _object, Point _position ){
        objects.push_back(ObjectMetadata(_object, _position));
    }
    vector<ObjectMetadata> getItems() { return objects; }
    void update(){return;}
private:
    int number;
    vector<ObjectMetadata> objects;
};