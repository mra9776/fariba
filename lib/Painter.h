#pragma once
#include <SFML/Graphics.hpp>
#include "ObjectMetadata.h"

class Painter
{
public:
    Painter(sf::RenderWindow* _window)
    {
        window = _window;
    }
    virtual void paint(ObjectMetadata *object) = 0;

    sf::RenderWindow* window;
    int WIDTH_MULTIPLIER = 100;
};