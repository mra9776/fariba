#pragma once

#include "Object.h"
#include "Point.h"
class Rock : public Object
{
public:
    Rock(const char *_name) : Object(_name)
    {
    }
    string getSign(){
        return "ROCK";
    }
};