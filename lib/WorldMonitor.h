#include <SFML/Graphics.hpp>
#include "World.h"
#include "PaintersHandler.h"

const int WIDTH_MULTIPLIR = 100;
class WorldMonitor{
    public:
    WorldMonitor(World* _world, sf::RenderWindow* _window){
        world = _world;
        window = _window;
        paintersHandler = new PaintersHandler(_window);
    }

    void render(){
        window->clear();
        for (ObjectMetadata object: world->getItems()){
            paintersHandler->paint(&object);
        }
        window->display();
    }
    private:
    World *world;   
    PaintersHandler *paintersHandler;
    sf::RenderWindow* window;
};