#pragma once

#include <string>
using std::string;

#include "Point.h"

class Object
{
public:
    Object(const char *_name)
    {
        name = string(_name);
    }

    string getName() { return name; }
private:
    string name;
};