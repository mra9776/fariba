#pragma once

class Point
{
public:
    Point(int _x, int _y)
    {
        x = _x;
        y = _y;
    }
    Point(const Point& _point)
    {
        x = _point.x;
        y = _point.y;
    }
    Point(){
        x = 0;
        y = 0;
    }
    int getX() { return x; };
    int getY() { return y; };

private:
    int x;
    int y;
};