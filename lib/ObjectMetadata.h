#pragma once

#include "Object.h"
#include "Point.h"

class ObjectMetadata{
public:
    ObjectMetadata(Object* _object, Point _position){
        item = _object;
        position = _position;
    }
    Object* item;
    Point position;
};